#!/bin/bash -e

## required for gitlab
git remote rm origin && git remote add origin git@gitlab.com:$CI_PROJECT_PATH.git # taken from https://forum.gitlab.com/t/git-push-from-inside-a-gitlab-runner/30554/4

git fetch

git checkout gitlab-pages
git merge origin/master -m "update changes from master"

if [ -n "$CI_COMMIT_REF_SLUG" ] && [ "$CI_COMMIT_REF_SLUG" != "master" ]
then
    echo deploying $CI_COMMIT_REF_SLUG version

    # overwrite latest stable version
    rm -r ./public/stable
    cp -r ./src/dev/ ./public/stable/ # this overwrites with the branch of the latest commit, we might want to skip it, if there is a newer version (branch name with greater date string)
    
    # overwrite latest branch version
    rm -r ./public/$CI_COMMIT_REF_SLUG/
    cp -r ./src/dev/ ./public/$CI_COMMIT_REF_SLUG/
else
    echo deploying dev version

    # overwrite latest dev version
    rm -r ./public/dev
    cp -r ./src/dev/ ./public/dev/
fi

git add .
git commit -a -m "deploy page for $CI_COMMIT_REF_SLUG"
git push



#### stuff we will do manually
# git branch stable/v2020-04-20


### some time later
# git checkout stable/v2020-04-20
## do some changes in src/dev
# git commit -a -m "fix in stable version"
# git push